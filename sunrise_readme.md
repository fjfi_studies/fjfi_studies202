# How to use this code on the sunrise cluster

## Accessing sunrise

The cluster sunrise.fjfi.cvut.cz is accessible using the usermap login credentials, however, the local account must be requested first. Access is provided via ssh:

`ssh <ctuid>@sunrise.fjfi.cvut.cz`

X forwarding is supported, you must have an XWindow client installed on your computer to use it. On Windows PuTTY is a good SSH client.

## Installing python into userspace

It seems that we need a userspace installation of python, otherwise `pip` didn't work. A way to get all python packages installed is to use Anaconda. Installation instructions are [here](https://docs.anaconda.com/free/anaconda/install/linux/), but the summary is the following:

1. Do ssh to the head node of sunrise
2. issue: `curl -O https://repo.anaconda.com/archive/Anaconda3-2023.09-0-Linux-x86_64.sh`
3. install with `sh ~/Downloads/Anaconda3-2020.05-Linux-x86_64.sh`
4. Default install to user home seems to work fine. Accepting auto-loading at login seems to be OK as well.

## Create a separate Anaconda environment

It is a good idea to create a separate environment for every project -- sometimes things can go wrong at packages can get entangled. The suggested environment is called `qrl`, the PBSPro job scripts will expect that this environment exists and is properly configured. The following steps achieve this

0. You may have to bring you Anaconda up-to-date first: `conda update -n base -c defaults conda`
1. `conda create -n qrl`
2. `conda activate qrl`
3. `conda install python=3.11` (Python 3.12 is installed by default and PyTorch does not yet support it)
4. `conda install pip`
5. `conda install matplotlib`
6. `pip install gym`
7. `pip install qutip`
8. `conda install pytorch torchvision torchaudio cpuonly -c pytorch`
9. `conda install tensorboardX`

Run `python ./setupcheck.py` then `python ./learning_tools.py` to verify if the basic packages are installed. To check the learning code you can switch to a branch with some testing maze

1. `git checkout test/8x8-model` (this has a maze file `maze_8x8.pkl`)
2. `qsub -I` to get interactive access to one of the computing nodes
3. `conda activate qrl`
4. Navigate to the code
5. `python ./learning_tools.py maze_8x8.pkl`

## Submitting a computation

All computation must be submitted to the job management system. Sunrise uses PBSPro or [OpenPBS](https://www.openpbs.org) (the two might be the same thing). The resources needed by a job are specified in a job script, which is essentially a shell script that is run at the client node, and the various parameters are supplied as 'comments' at the top of the job script. There is a job script we can use as a starting point: `qsub_learning_tools.sh`
