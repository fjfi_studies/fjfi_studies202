# Research project repository
This is the Gitlab repository of the research project (a "microthesis" between bachelor and master thesis) by Vlastimil Hudeček at CTU FNSPE. The content of this repository may be updated.

## Content of repository
* PDF file of the research project text
* TeXmacs file of the research project text
* Jupyter notebooks which were used for the calculations
* Related documents and source codes