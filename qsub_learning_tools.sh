#!/bin/sh

# Run e.g. with the command:
# qsub qsub_learning_tools.sh

#PBS -N learning_tools_job
#PBS -l select=1:ncpus=4:mem=26gb
#PBS -l walltime=23:59:00
#:PBS -l walltime=96:00:00
### Send email on abort or end
#:PBS -m ae
#:PBS -M gabris.aurel@fjfi.cvut.cz

MAZE_FILE=maze_8x8.pk

# Switch to the 'qrl' conda environment
source ${HOME}/anaconda3/bin/activate qrl
conda init

# Switch to the directory where qsub was launched
if [ ! -z ${PBS_O_WORKDIR} ]; then cd $PBS_O_WORKDIR;fi

python ./learning_tools.py ${MAZE_FILE}
